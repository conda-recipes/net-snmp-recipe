# net-snmp conda recipe

Home: "http://www.net-snmp.org/"

Recipe license: BSD 3-Clause

Summary: Net-SNMP is a suite of applications used to implement SNMP v1, SNMP v2c and SNMP v3 using both IPv4 and IPv6.
